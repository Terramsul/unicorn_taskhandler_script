#!/bin/bash

############   VARIABLES   ##########################################
shpath=/home/limesuser
update_file=/home/limesuser/scheduler/update.task
compose_file=/home/limesuser/scheduler/compose.task
cron_job="*/5 *    * * *  limesuser /home/limesuser/taskhandler.sh > /dev/null 2>&1"
docker_file=/home/limesuser/scheduler/docker.task
dlurl=https://limes.rocks/limes/docker/backend_script
backup_file=/home/limesuser/scheduler/backup.task
taskhandler=/home/limesuser/scheduler/taskhandler.task
restore=/home/limesuser/scheduler/restore.task
container1=db_server*
container2=nodejs*
container3=limesuser_nginx*
source /home/limesuser/.ssh/secure/dat.conf
############ SCRIPT FUNCTIONS  ######################################
echo ${USER}
function jumpto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

start=${1:-"start"}
regular=${1:-"regular"}
dockercompose=${1:-"dockercompose"}
xchange=${1:-"xchange"}

############   ERROR HANDLING  ######################################

directory=$(pwd)
if [ $directory != "/home/limesuser" ] ; 
	then	
		echo "Error! Script is executed in wrong directory!"
		exit 1
	else
		echo "Script is in right Directory"
		jumpto $start
fi

sudo chmod -vfR 777 /home/limesuser/*.yml
sudo chmod -vfR 777 /home/limesuser/scheduler/*.task

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#   						HANDS OFF !!!			   				#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

start:
############   FIRST RUN       ######################################
	mkdir -p $shpath/backup
	mkdir -p $shpath/scheduler
	mkdir -p $shpath/data
	mkdir -p $shpath/database


if grep -q "/home/limesuser/taskhandler.sh" /etc/crontab;
	then
		jumpto $dockercompose
	else 
		echo "$cron_job"
		echo -e "$cron_job" >> /etc/crontab
		jumpto $start
fi

dockercompose:

if [ -e docker-compose.yml ] ;
	then 
			jumpto $regular
	else 
		wget $dlurl/docker-compose.yml
		echo "docker-compose downloaded!"
	jumpto $start
fi
	
############    REGULAR RUN		#####################################

regular:
rm $shpath/scheduler/state.json -y
echo "1"
wget $dlurl/state.json
mv  -f state.json $shpath/scheduler

#check for running Docker Containers

RUNNING=$(docker inspect --format="{{.State.Running}}" $container1 2> /dev/null)
if [ $? -eq 1 ]; then
  docker-compose up -d
fi
if [ "$RUNNING" == "false" ]; then
  docker-compose up -d
  echo ${container1}" started"
fi
RUNNING=$(docker inspect --format="{{.State.Running}}" $container2 2> /dev/null)
if [ "$RUNNING" == "false" ]; then
  docker-compose up -d
  echo ${container2}" started"
fi
RUNNING=$(docker inspect --format="{{.State.Running}}" $container3 2> /dev/null)
if [ "$RUNNING" == "false" ]; then
  docker-compose up -d
  echo ${container3}" started"
fi

#Taskhandling

if [ -e $update_file ] ;
	then 
		#execution of Docker commands
		docker-compose pull		
		docker-compose up -d
		#adding file to list for beeing deleted
		echo ${update_file}>>data.remove	
		#echoing state of Task	
		echo "Update Task completed"		
	else 
		#echoing state of Task
		echo "Up to date" 	
fi

if [ -e $backup_file ] ;
	then 
		./incbak.sh
		echo `date +%d-%m-%Y"_"%H_%M_%S`>$shpath/scheduler/backup.done
		echo ${backup_file}>>data.remove
fi

if [ -e $docker_file ] ;
	then
		rm $shpath/docker-compose.yml
		wget $dlurl/docker-compose.yml
		echo ${docker_file}>>data.remove
fi

if [ -e $taskhandler ] ;
	then 
		wget $dlurl/taskhandler.new
		.$shpath/taskhandlerxchange.sh
		echo ${taskhandler}>>data.remove
fi

if [ -e $restore ] ;
	then 
		./restore.sh
		echo "Backup restored"
fi


xchange:
#./$directory/sftp_backup.sh

#deleting Files listed in data.remove

for f in $(cat $shpath/data.remove) ; do 
	rm "$f"
done 

rm -f $shpath/data.remove
