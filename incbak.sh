
if mount | grep /home/limesuser/SFTPBackup > /dev/null; then
    echo "Drive allrdy mounted."
else
    mount /home/limesuser/SFTPBackup
fi

source=/home/limesuser/      # Source Folder (Unicorn)
backup=/home/limesuser/SFTPBackup      # Target Folder
exclude=/home/limesuser/SFTPBackup/exclude.txt # excluded Files 

touch $exclude

weekday=$(date +"%a") # %a = Weekday as Txt
day=$(date +"%d")       # %d = date as two digit number 
month=$(date +"%m")     # %m = month as two digit number 

function backup ()
{
 rsync -rtpgov --delete --checksum -hh --stats --exclude-from="$exclude" --link-dest="$source/" "$source/" "$source//" >"$source/scheduler/_last_incback.txt" 2>&1
 echo -e '\n'-- '\n'last Backup: $(date "+%Y-%m-%d %H:%M:%S") >> "$$source/scheduler/_last_incback.txt"
 mv $source/scheduler/_last_incback.txt $source/scheduler/_last_incback_$(date "+%Y-%m-%d").txt
}

## Daily Backup:
if [[ $day != 01 && $day != 09 && $day != 16 && $day != 24 ]]; then
 # Backup in Folder "Day" 
 case "$weekday" in Mo|Di|Mi|Do|Fr|Sa|So) backup $weekday ;; esac
fi

## Weekly Backup:
case "$day" in 09) backup 09 ;; esac
case "$day" in 16) backup 16 ;; esac
case "$day" in 24) backup 24 ;; esac

## Monthly Backup:
case "$month" in 02|04|06|08|10|12)
case "$day" in 01)
 backup Mg
 ;; esac
;; esac
case "$month" in 01|03|05|07|09|11)
case "$day" in 01)
 backup Mu
 ;; esac
;; esac
# case "$day" in 01) backup Monat_$monat ;; esac
# case "$day" in 01) case "$month" in 01) backup Jahr_$(date +"%Y") ;; esac ;; esac